// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.issuetestpythonfunction = require("./issuetestpython/function.js").handler;
exports.issuetestpythonpyfirst = require("./issuetestpython/pyfirst.py").handler;